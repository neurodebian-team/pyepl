# PyEPL: hardware/eeg/pulse/__init__.py
#
# Copyright (C) 2003-2005 Michael J. Kahana
# Authors: Ian Schleifer, Per Sederberg, Aaron Geller, Josh Jacobs
# URL: http://memory.psych.upenn.edu/programming/pyepl
#
# Distributed under the terms of the GNU Lesser General Public License
# (LGPL). See the license.txt that came with this file.

import sys
from pulseexc import EPLPulseEEGException
if sys.platform=='darwin':
    from awCard import awCard
else:
    from parallel import Parallel


