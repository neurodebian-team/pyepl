// PyEPL: hardware/eeg/pulse/eplPulse.c
//
// Copyright (C) 2003-2005 Michael J. Kahana
// Authors: Ian Schleifer, Per Sederberg, Aaron Geller, Josh Jacobs
// URL: http://memory.psych.upenn.edu/programming/pyepl
//
// Distributed under the terms of the GNU Lesser General Public License
// (LGPL). See the license.txt that came with this file.

#include "eplPulse.h"

// File descriptor
int fd = -1;

int eplOpenPort()
{
  int failure = 0;
  fd = open("/dev/parport0",O_WRONLY,0);
  
  if (fd != -1)
  {
    // claim the port
    failure = ioctl(fd,PPCLAIM);

    // close if failed
    if (failure)
    {
      // close the port
      eplClosePort();
    }
  }
  else
  {
    failure = -1;
  }

  return failure;
}

void eplClosePort()
{
  if (fd != -1)
  {
    // release the port
    ioctl(fd, PPRELEASE);

    // close the port
    close(fd);
    fd = -1;
  }
  
}


int eplSetPortState(int state)
{
  struct ppdev_frob_struct frob;
  unsigned char data;

  int failure = 0;

  // standard mask
  frob.mask = PARPORT_CONTROL_STROBE;

  // check what state to set
  if (state == 0)
  {
    // turn it off
    frob.val = 0;
    data = 0;
  }
  else
  {
    // turn it on
    frob.val = PARPORT_CONTROL_STROBE;
    data = 0xFF;
  }

  // talk to the port
  failure = ioctl(fd, PPWDATA, &data);
  if (!failure)
  {
    failure = ioctl(fd, PPFCONTROL, &frob);
  }

  return failure;
  
}
