# PyEPL: hardware/eeg/pulse/pulse.pyx
#
# Copyright (C) 2003-2005 Michael J. Kahana
# Authors: Ian Schleifer, Per Sederberg, Aaron Geller, Josh Jacobs
# URL: http://memory.psych.upenn.edu/programming/pyepl
#
# Distributed under the terms of the GNU Lesser General Public License
# (LGPL). See the license.txt that came with this file.

"""
Module to send synch pulses out the parallel port.
"""

# Sync Pulse Module
from pulseexc import EPLPulseEEGException

# file descriptor object
#cdef object f

cdef extern int eplOpenPort()
cdef extern void eplClosePort()
cdef extern int eplSetPortState(int state)

def openPort():
    """
    Opens the /dev/parport file object
    """
    #f = open("/dev/parport0","w")
    failure = eplOpenPort()
    if failure:
        raise EPLPulseEEGException("Couldn't open parport.")
    
def closePort():
    """
    Closes the /dev/parport file object
    """
    #f.close()
    eplClosePort()

def setState(state):
    """
    Turns the pulse on(1) or off(0) depending on the state parameter.
    """
    failure = eplSetPortState(state)
    if failure:
        raise EPLPulseEEGException("Couldn't set parport state.")

def setSignal(signal):
    """
    Sends a specific signal to the parpoprt.
    """
    raise EPLPulseEEGException("Not implemented yet.")
