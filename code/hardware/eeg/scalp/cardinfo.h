// PyEPL: hardware/eeg/scalp/cardinfo.h
//
// Copyright (C) 2003-2005 Michael J. Kahana
// Authors: Ian Schleifer, Per Sederberg, Aaron Geller, Josh Jacobs
// URL: http://memory.psych.upenn.edu/programming/pyepl
//
// Distributed under the terms of the GNU Lesser General Public License
// (LGPL). See the license.txt that came with this file.

#ifndef _CARDINFO_H_
#define _CARDINFO_H_

/*--------------------- Share Memory Declaration ----------------------------*/
#define S_S 2                  	/* sample size */
/*--------------------- Channel Information ---------------------------------*/
#define NCARDS 1                /* total number of cards in system           */
#define CH_PER_CARD 64          /* total channels per card                   */
#define CH NCARDS*CH_PER_CARD   /* total number of channels recording from   */
/* -------------------- Sampling Rates --------------------------------------*/
#define SR 256			/* Sampling Rate                             */

#endif


