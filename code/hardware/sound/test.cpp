// PyEPL: hardware/sound/test.cpp
//
// Copyright (C) 2003-2005 Michael J. Kahana
// Authors: Ian Schleifer, Per Sederberg, Aaron Geller, Josh Jacobs
// URL: http://memory.psych.upenn.edu/programming/pyepl
//
// Distributed under the terms of the GNU Lesser General Public License
// (LGPL). See the license.txt that came with this file.

extern "C" void play_wave();
extern "C" void eplSound_wcreate(long,long);
extern "C" void client_code();
extern "C" void eplSound_wdelete();

int main(int argc, char *argv[]){
  if (argc==2){
    int size = 20*2*44100;
    eplSound_wcreate(size,size);
    switch(atoi(argv[1])){
    case 1:
      play_wave();
      break;
    case 2:
      client_code();
      break;
    }
  }
}

