// PyEPL: hardware/sound/eplSound.cpp
//
// Copyright (C) 2003-2005 Michael J. Kahana
// Authors: Ian Schleifer, Per Sederberg, Aaron Geller, Josh Jacobs
// URL: http://memory.psych.upenn.edu/programming/pyepl
//
// Distributed under the terms of the GNU Lesser General Public License
// (LGPL). See the license.txt that came with this file.

#include "eplSound.h"

#include <iostream>
#include <cstdlib>
#include <cstring>

using namespace std;

eplSound::eplSound(long recLen, long playLen, 
		   unsigned int sampRate, unsigned int bufSize)
{
  // get audio instance
  RtAudio audio;
  playAudio = NULL;
  recAudio = NULL;

  // let's see warnings
  audio.showWarnings( true );

  // make sure there are audio devices
  if ( audio.getDeviceCount() < 1 ) 
  {
    cerr << "\nNo audio devices found!\n";
    exit( EXIT_FAILURE );
  }

  // must find default input/output device/s and channels
  playDevice = 0;
  recDevice = 0;
  playChans = 0;
  recChans = 0;

  // set some vars to defaults
  sampleRate = sampRate;
  bufferSize = bufSize;

  // set up the stream parameters
  RtAudio::StreamParameters iParams, oParams;
  RtAudio::StreamOptions options;

  // set the options
  //options.flags |= RTAUDIO_HOG_DEVICE;
  options.flags |= RTAUDIO_SCHEDULE_REALTIME;

  // Verify that the default devices will work for us
  RtAudio::DeviceInfo info;
  
  // Check and set up the play device
  try {
    playDevice = audio.getDefaultOutputDevice();
    info = audio.getDeviceInfo( playDevice );
    oParams.deviceId = playDevice;
    oParams.nChannels = info.outputChannels;
    playChans = oParams.nChannels;
  }
  catch ( RtError& e ) {
    e.printMessage();
    //exit( EXIT_FAILURE );
  }

  // Check and set up the rec device
  try {
    recDevice = audio.getDefaultInputDevice();
    info = audio.getDeviceInfo( recDevice );
    iParams.deviceId = recDevice;
    iParams.nChannels = info.inputChannels;
    recChans = iParams.nChannels;
  }
  catch ( RtError& e ) {
    e.printMessage();
    //exit( EXIT_FAILURE );
  }

  // allocate space for data
  data = new audioBuffer(recLen,playLen,
			 recChans,playChans,
			 sampleRate);

  try {
    // open the proper stream
    if ((playChans>0) && (recChans>0) && (iParams.deviceId == oParams.deviceId))
    {
      // it's duplex
      isDuplex = 1;
      
      // open both
      playAudio = new RtAudio();
      playAudio->showWarnings( true );
      playAudio->openStream( &oParams, &iParams, FORMAT, sampleRate, 
			    &bufferSize, &inout, (void *)data );
      recAudio = playAudio;
    }
    else 
    {
      // not full duplex
      isDuplex = 0;

      if (playChans>0)
      {
	// open output
	playAudio = new RtAudio();
	playAudio->showWarnings( true );
	playAudio->openStream( &oParams, NULL, FORMAT, sampleRate, 
			       &bufferSize, &inout, (void *)data );
      }
      if (recChans>0)
      {
	// open input
	recAudio = new RtAudio();
	recAudio->showWarnings( true );
	recAudio->openStream( NULL, &iParams, FORMAT, sampleRate, 
			      &bufferSize, &inout, (void *)data );	
      }
    }

    if (playChans==0)
    {
      // No input chans discovered
      cerr << "No default input device with correct channel info was found!" << endl;
      cerr << "You will only be able to record sound." << endl;
    }
    if (recChans==0)
    {
      // No output chans discovered
      cerr << "No default output device with correct channel info was found!" << endl;
      cerr << "You will not be able to play sound." << endl;
    }
  }
  catch ( RtError& e ) {
    e.printMessage();
    exit( EXIT_FAILURE );
  }

  // set to be not streaming yet
  streaming = 0;
}

eplSound::~eplSound()
{
  // stop streaming
  stopstream(1);

  // close the play stream
  if (playAudio != NULL)
  {
    if (playAudio->isStreamOpen() > 0)
      playAudio->closeStream();

    // clean it up
    delete playAudio;
  }

  // see if should clean up record stream
  if ((!isDuplex) && (recAudio != NULL))
  {
    if (recAudio->isStreamOpen() > 0)
      recAudio->closeStream();

    // clean it up
    delete recAudio;
  }
  
  delete data;
}

void eplSound::clear()
{
  data->playBuf->clear();
  data->recBuf->clear();
  return;
}

void eplSound::clearPlayBuffer()
{
  data->playBuf->clear();
  return;
}

void eplSound::clearRecBuffer()
{
  data->recBuf->clear();
  return;
}

int eplSound::recstart()
{

  // first clear (is there any case where we don't want to do this?)
  data->recBuf->clear();

  // set to recording
  data->recording = 1;

  // must start streaming (will only start if not already started)
  startstream();

  return 0;
}

int eplSound::recstop()
{
  // stop recording
  data->recording = 0;

  return 0;
}

int eplSound::startstream()
{
  //if (!(audio.isStreamOpen()) && (streaming == 0))
  if (streaming == 0)
  {
    // start playing
    if (playAudio != NULL)
    {
      try {
	playAudio->startStream();
      }
      catch (RtError &error) {
	error.printMessage();
	exit(EXIT_FAILURE);
      }
    }

    if ((recAudio != NULL) && (recAudio != playAudio))
    {
      try {
	recAudio->startStream();
      }
      catch (RtError &error) {
	error.printMessage();
	exit(EXIT_FAILURE);
      }
    }

    // say we are streaming
    streaming = 1;
  }

  return 0; 
}

int eplSound::stopstream(int abort)
{
  //if (audio.isStreamOpen() && streaming == 1))
  if (streaming == 1)
  {
    // stop playing
    if (playAudio != NULL)
    {
      try {
	if (abort)
	  playAudio->abortStream();
	else
	  playAudio->stopStream();
      }
      catch (RtError &error) {
	error.printMessage();
	exit(EXIT_FAILURE);
      }
    }

    // stop playing
    if ((recAudio != NULL) && (recAudio != playAudio))
    {
      try {
	if (abort)
	  recAudio->abortStream();
	else
	  recAudio->stopStream();
      }
      catch (RtError &error) {
	error.printMessage();
	exit(EXIT_FAILURE);
      }
    }

    streaming = 0;
    data->recording = 0;
  }

  return 0; 
}

long eplSound::append(MY_TYPE *newdata, long length, int overwrite, float ampFactor)
{
  long i;

  // apply amplification if necessary
  if (ampFactor != 1.0)
  {
    // Apply the factor
    for (i=0;i<length;i++)
    {
      newdata[i]= (MY_TYPE)(newdata[i]*ampFactor);
    }
  }
  // append to the play buffer
  return data->playBuf->append(newdata,length,overwrite);
}

long eplSound::consume(MY_TYPE *newdata, long length)
{
  // consume from the recBuf
  return data->recBuf->consume(newdata,length);
}

int eplSound::getBufferSize()
{
  return bufferSize;
}

long eplSound::getSamplesPlayed()
{
  return data->samplesPlayed;
}

void eplSound::resetSamplesPlayed()
{
  data->samplesPlayed = 0;
}

int eplSound::getRecChans()
{
  return recChans;
}

int eplSound::getPlayChans()
{
  return playChans;
}

int eplSound::getSampleRate()
{
  return sampleRate;
}

unsigned int eplSound::getPlayStreamSampleRate()
{
  return playAudio->getStreamSampleRate();
}

unsigned int eplSound::getRecStreamSampleRate()
{
  return recAudio->getStreamSampleRate();
}

long eplSound::getBufferUsed()
{
  return data->playBuf->getUsed();
}

long eplSound::getPlayStreamLatency()
{
  return playAudio->getStreamLatency();
}

long eplSound::getRecStreamLatency()
{
  return recAudio->getStreamLatency();
}

int inout( void *outputBuffer, void *inputBuffer, unsigned int nBufferFrames,
           double streamTime, RtAudioStreamStatus status, void *data )
{
  audioBuffer *mydata = (audioBuffer *)data;
  MY_TYPE *inbuffer = (MY_TYPE *) inputBuffer;
  MY_TYPE *outbuffer = (MY_TYPE *) outputBuffer;
  unsigned int written = 0;

  // check the status
  if ( status )
    cerr << "Stream overflow detected!" << endl;
 
  // handle recording
  if ((inputBuffer != NULL) && (mydata->recording))
  {
    // read in from buffer to recBuf
    mydata->recBuf->append(inbuffer,nBufferFrames*mydata->recChans);
  }

  // handle playing
  if (outputBuffer != NULL)
  {
    // write from playBuf to buffer
    written =  mydata->playBuf->consume(outbuffer,nBufferFrames*mydata->playChans);

    // Append samples played
    mydata->samplesPlayed += written/(mydata->playChans);

    // if we wrote less than buffer, append silence
    if (written < nBufferFrames * mydata->playChans)
    {
      // append zeros
      memset((void *)&outbuffer[written],0,
	     sizeof(MY_TYPE)*((nBufferFrames*mydata->playChans)-written));
    }
  }

  return 0;
}


audioBuffer::audioBuffer(long reclen, long playlen, 
			 unsigned int inrecChans, unsigned int inplayChans, 
			 unsigned int samprate)
{
  // Allocate space for the two buffers
  recBuf = new fifo(reclen*inrecChans*samprate);
  playBuf = new fifo(playlen*inplayChans*samprate);
  
  //chans = numchans;
  recChans = inrecChans;
  playChans = inplayChans;
  rate = samprate;

  recording = 0;
  samplesPlayed = 0;
}

audioBuffer::~audioBuffer()
{
  // clear the buffer memory
  delete recBuf;
  delete playBuf;
}

